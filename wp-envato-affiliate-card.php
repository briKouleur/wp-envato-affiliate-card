<?php
/**
 * Plugin Name: WP Envato Affiliate Card by b*web
 * Plugin URI: http://b-website.com/wp-plugin-info-card-for-wordpress
 * Description: WP Envato Affiliate Card displays nice cards with Envato API data and lets you add your affiliate ID to get commissions.
 * Author: Brice CAPOBIANCO
 * Author URI: http://b-website.com/
 * Version: 1.0.10
 * Domain Path: /langs
 * Text Domain: wppic-envato
 */


/***************************************************************
 * SECURITY : Exit if accessed directly
***************************************************************/
if ( !defined( 'ABSPATH' ) ) {
	die( 'Direct acces not allowed!' );
}


/***************************************************************
 * Define constants
 ***************************************************************/
if ( !defined( 'WPPIC_ID' ) ) {
	define( 'WPPIC_ID', 'wp-plugin-info-card' ); 
}


/***************************************************************
 * Load plugin textdomain
 ***************************************************************/
function wppic_envato_load_textdomain() {
	$path = dirname( plugin_basename( __FILE__ ) ) . '/langs/';
	load_plugin_textdomain( 'wppic-envato', false, $path );
}
add_action( 'init', 'wppic_envato_load_textdomain' );


/***************************************************************
 * Load hooks when all plugins are loaded
***************************************************************/
function wppic_envato_requirement() {
    $url = network_admin_url( 'plugin-install.php?tab=search&type=term&s=WP+Plugin+Info+Card&plugin-search-input=Search+Plugins' );
    echo '
    <div class="error">
        <p><a href="' . $url . '">WP Plugin Info Card</a> is required to use <strong>WP Envato Affiliate Card</strong>.</p>
    </div>
    ';
}


/***************************************************************
 * Add settings link on plugin list page
 ***************************************************************/
function wppic_envato_settings_link( $links ) { 
  $links[] = '<a href="admin.php?page=' . WPPIC_ID . '">' . __( 'Settings', 'wppic-translate' ) . '</a>'; 
  return $links; 
}


/***************************************************************
 * Add custom meta link on plugin list page
 ***************************************************************/
function wppic_envato_meta_links( $links, $file ) {
	if ( $file === 'wp-envato-affiliate-card/wp-envato-affiliate-card.php' ) {
		$links[] = '<a href="http://b-website.com/wp-plugin-info-card-for-wordpress" target="_blank" title="'. __( 'Documentation and examples', 'wppic-translate' ) .'"><strong style="color:#db3939">'. __( 'Documentation and examples', 'wppic-translate' ) .'</strong></a>';
		$links[] = '<a href="http://b-website.com/category/plugins" target="_blank" title="'. __( 'More b*web Plugins', 'wppic-translate' ) .'">'. __( 'More b*web Plugins', 'wppic-translate' ) .'</a>';
	}
	return $links;
}


/***************************************************************
 * Load hooks when all plugins are loaded
***************************************************************/
function wppic_envato_affiliate_card() {

	//check if WPPIC is activated
    if ( current_user_can( 'activate_plugins' ) ) {
        if ( !defined('WPPIC_PATH' ) ) {
			add_action( 'admin_notices', 'wppic_envato_requirement' );
		} else {
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wppic_envato_settings_link' );
			add_filter( 'plugin_row_meta', 'wppic_envato_meta_links', 10, 2 );
		}
    }
	add_action( 'wppic_enqueue_scripts', 'wppic_envato_enqueue_sripts', 99 );
	add_filter( 'wppic_add_api_parser', 'wppic_envato_api_parser', 9, 3 );
	add_filter( 'wppic_add_template', 'wppic_envato_template', 9, 2 );
	add_filter( 'wppic_add_mce_type', 'wppic_envato_mce_type' );
	add_filter( 'wppic_add_list_form', 'wppic_envato_list_form' );
	add_filter( 'wppic_add_widget_type', 'wppic_envato_widget_type' );
	add_filter( 'wppic_add_list_valdiation', 'wppic_envato_list_valdiation' );
	add_filter( 'wppic_add_widget_item', 'wppic_envato_widget_item', 9, 3 );
	add_action( 'admin_init', 'wppic_envato_settings', 10 );

}
add_action( 'plugins_loaded', 'wppic_envato_affiliate_card' );


/***************************************************************
 * Enqueue Scripts
 ***************************************************************/
function wppic_envato_enqueue_sripts() {
	wp_enqueue_style( 'wppic-envato-style', plugin_dir_url( __FILE__ ) . 'css/wppic-envato-style.min.css', NULL, NULL );
}


/***************************************************************
 * Fetching product data through Envato API
 ***************************************************************/
function wppic_envato_api_parser( $wppic_data, $type, $slug ){

	if ( $type == 'envato' ) {

		$response = wp_remote_get( 'http://marketplace.envato.com/api/edge/item:' . $slug . '.json', array( 'timeout' => 10 ) );
		$envatoApi = wp_remote_retrieve_body( $response);
		$envatoApi = json_decode( $envatoApi);
		$envatoApi = $envatoApi->item;
		
		if(wp_remote_retrieve_response_code( $response ) == '200' && !empty( $envatoApi->item ) ){
			$envatoApiUser = wp_remote_get( 'http://marketplace.envato.com/api/edge/user:' . $envatoApi->user . '.json', array( 'timeout' => 10 ) );
			$envatoApiUser = wp_remote_retrieve_body( $envatoApiUser );
			$envatoApiUser = json_decode( $envatoApiUser );
			$envatoApiUser = $envatoApiUser->user;
		} else {
			//no result
			return $wppic_data  = false;
		}

		switch ( true ) {
			case strstr( $envatoApi->url,'http://themeforest.net/' ):
				$market = 'themeforest';
				break;
			case strstr( $envatoApi->url,'http://codecanyon.net/' ):
				$market = 'codecanyon';
				break;
			case strstr( $envatoApi->url,'http://videohive.net/' ):
				$market = 'videohive';
				break;
			case strstr( $envatoApi->url,'http://audiojungle.net/' ):
				$market = 'audiojungle';
				break;
			case strstr( $envatoApi->url,'http://graphicriver.net/' ):
				$market = 'graphicriver';
				break;
			case strstr( $envatoApi->url,'http://photodune.net/' ):
				$market = 'photodune';
				break;
			case strstr( $envatoApi->url,'http://3docean.net/' ):
				$market = '3docean';
				break;
			case strstr( $envatoApi->url,'http://activeden.net/' ):
				$market = 'activeden';
				break;
			default:
				$market = '';
		}

		//define product thumbnail as thumbnail if no preview image is set
		if( isset( $envatoApi->live_preview_url ) ) {
			$live_preview_url = $envatoApi->live_preview_url;
		} else {
			$live_preview_url = '';
		}
		
		//Check if media AUDIO exists
		if( isset( $envatoApi->preview_type ) && $envatoApi->preview_type == 'audio' ) {
			$audio_url = $envatoApi->preview_url;
		} else {
			$audio_url = '';
		}
		
		//Check if video URL exists
		if( isset( $envatoApi->live_preview_video_url ) ) {
			$video_url = $envatoApi->live_preview_video_url;
		} else {
			$video_url = '';
		}
		
		$wppic_data  = (object) array( 
			'slug' 				=> $slug,
			'url'				=> $envatoApi->url,
			'market'			=> $market,
			'name' 				=> $envatoApi->item,
			'cost' 				=> number_format( $envatoApi->cost, 0, ',', ',' ),
			'author' 			=> $envatoApi->user,
			'avatar' 			=> $envatoApiUser->image,
			'followers' 		=> number_format( $envatoApiUser->followers, 0, ',', ',' ),
			'sales' 			=> number_format( $envatoApiUser->sales, 0, ',', ',' ),
			'live_preview_url'	=> $live_preview_url,
			'thumbnail'			=> $envatoApi->thumbnail,
			'rating' 			=> number_format( ( $envatoApi->rating_decimal/5 ) * 100, 1, ',', ',' ),
			'sales' 			=> number_format( $envatoApi->sales, 0, ',', ','),
			'last_updated' 		=> $envatoApi->last_update,
			'audio_url' 		=> $audio_url,
			'video_url' 		=> $video_url,		
		);
	
	}
	
	return $wppic_data;
	
}


/***************************************************************
 * Envato shortcode template preparation
 ***************************************************************/
function wppic_envato_template( $content, $data ){
	
	$type = $data[0];
	$wppic_data = $data[1];
	$image = $data[2];
	$layout = '-' . $data[3];

	if ( $type == 'envato') {

		//Load custom user template if exists
		$WPPICtemplatefile = '/wppic-templates/wppic-template-envato';
		ob_start();
		if ( file_exists( get_template_directory() . $WPPICtemplatefile .  $layout . '.php' ) ) { 
			include( get_template_directory() . $WPPICtemplatefile .  $layout . '.php' ); 
		} else if ( file_exists( plugin_dir_path( __FILE__ )  . $WPPICtemplatefile .  $layout . '.php' ) ) { 
			include( plugin_dir_path( __FILE__ )  . $WPPICtemplatefile .  $layout . '.php' ); 
		} else {
			include( plugin_dir_path( __FILE__ )  . $WPPICtemplatefile . '.php' ); 
		}
		$content .= ob_get_clean();

	}
	
	return $content;
	
}


/***************************************************************
 * New option field for Envato username
 ***************************************************************/
function wppic_envato_settings(){
	add_settings_field(
		'wppic-envato-id',
		__( 'Envato username', 'wppic-envato' ), 
		'wppic_envato_id',
		WPPIC_ID . 'options',
		'wppic_options'
	);
}


/***************************************************************
 * Input option for the new option field
 ***************************************************************/
function wppic_envato_id() {
	$wppicSettings = get_option( 'wppic_settings' );
	
	$envatoID = '';
	if( isset ( $wppicSettings[ 'envato-id' ] ) ) {
		$envatoID = $wppicSettings[ 'envato-id' ];
	}
	
	$content = '<td>';
		$content .= '<input type="text" id="wppic-envato-id" name="wppic_settings[envato-id]" value="' . $envatoID . '">';
		$content .= '<label for="wppic-envato-id">' . __('Your Envato affiliate ID/username', 'wppic-envato') . '</label>';
	$content .= '</td>';
	
	echo $content;
	
}


/***************************************************************
 * Input option for the envato item list
 ***************************************************************/
function wppic_envato_list_form( $parameters ){
	$parameters[] = array( 'envato-list', __( 'Add an Envato product', 'wppic-envato' ), __( 'Please refer to the Envato item URL to determine the product ID', 'wppic-envato' ), 'http://themeforest.net/item/item-name/<strong>ITEM-ID</strong>' );
	return $parameters;
}


/***************************************************************
 * Envato input validation
 ***************************************************************/
function wppic_envato_list_valdiation( $parameters ){
	$parameters[] = array('envato-list', __( 'is not a valid item ID. This key has been deleted.' , 'wppic-envato' ), '/^[0-9]+$/' );
	return $parameters;
}


/***************************************************************
 * Add envato type to mce list
 ***************************************************************/
function wppic_envato_mce_type( $parameters ){
	$parameters[ 'types' ][] = array( 'text' => 'Envato', 'value' => 'envato' );
	return $parameters;
}


/***************************************************************
 * Envato widget list
 ***************************************************************/
function wppic_envato_widget_type( $parameters ){
	$parameters[] = array( 'envato', 'envato-list', __( 'Envato', 'wppic-envato' ) );
	return $parameters;
}


/***************************************************************
 * Envato widget item render
 ***************************************************************/
function wppic_envato_widget_item( $content, $wppic_data, $type ){
	if( $type == 'envato' ){
		//Date format Internationalizion
		global 	$wppicDateFormat;
		$wppic_data->last_updated = date_i18n( $wppicDateFormat, strtotime( $wppic_data->last_updated ) );

		$content .= '<div class="wp-pic-item">';
		$content .= '<a class="wp-pic-widget-name" href="' . $wppic_data->url . '" target="_blank" title="' . __('Visit Product Page', 'wppic-envato') . '">' . $wppic_data->name .'</a>';
		$content .= '<span class="wp-pic-widget-rating"><span>' . __('Ratings:', 'wppic-envato') . '</span> ' . $wppic_data->rating .'%';
		if( !empty( $wppic_data->num_ratings ) )
			$content .= ' (' . $wppic_data->num_ratings . ' votes)';
		$content .= '</span>';
		$content .= '<span class="wp-pic-widget-downloaded"><span>' . __('Sales:', 'wppic-envato') . '</span> ' . $wppic_data->sales .'</span>';
		$content .= '<p class="wp-pic-widget-updated"><span>' . __('Last Updated:', 'wppic-envato') . '</span> ' . $wppic_data->last_updated;
		if( !empty( $wppic_data->version ) )
			$content .= ' (v.' . $wppic_data->version .')';
		$content .= '</p>';
		$content .= '</div>';
	}
	return $content;
}