<?php
/***************************************************************
 * $wppic_data Object contain the following values: 
 * slug, url, name, market, cost, author, avatar, followers, sales, live_preview_url, thumbnail, rating, downloaded, last_updated, audio_url, video_url
 ***************************************************************/

$wppicSettings = get_option( 'wppic_settings' );

//Add username param to product URL
if( !isset ( $wppicSettings[ 'envato-id' ] ) || empty( $wppicSettings[ 'envato-id' ] ) ) {
	$wppic_data->url = $wppic_data->url . '?ref=b-web';
} else {
	$wppic_data->url = $wppic_data->url . '?ref=' . $wppicSettings[ 'envato-id' ];
}


//Construct Author link
$authorUrl = parse_url($wppic_data->url);
if( !isset ( $wppicSettings[ 'envato-id' ] ) || empty( $wppicSettings[ 'envato-id' ] ) ) {
	$authorUrl = $authorUrl[ 'scheme' ] . '://' . $authorUrl[ 'host' ] . '/user/' . $wppic_data->author . '?ref=b-web'; 
} else {
	$authorUrl = $authorUrl[ 'scheme' ] . '://' . $authorUrl[ 'host' ] . '/user/' . $wppic_data->author . '?ref=' . $wppicSettings[ 'envato-id' ];
}

//Define card image
//$image is the custom image URL if you provided it
if( !empty( $image ) ){
	$bgImage = 'style="background-image: url(' . $image . ');"';
} else if( !empty( $wppic_data->live_preview_url ) && empty( $wppic_data->audio_url ) ) {
	$bgImage = 'style="background-image: url(' . esc_attr( $wppic_data->live_preview_url ) . ');"';
} else {
	$bgImage = 'data="no-image" style="background-image: url(' . esc_attr( $wppic_data->thumbnail ) . ');"';
}

//Plugin banner
$banner = '';
if( !empty( $image ) ){
	$banner = '<img src="' . $image . '" alt="' . $wppic_data->name . '" />';
} 
if( !empty($wppic_data->live_preview_url) && empty( $wppic_data->audio_url ) ) {
	$banner = '<img src="' . $wppic_data->live_preview_url . '" alt="' . $wppic_data->name . '" />';
}

		
//Media embed
$mediaType = '';
$media = '';
if( !empty( $wppic_data->audio_url ) ){
	$media = wp_audio_shortcode( array(
		'src'      	=> $wppic_data->audio_url,
		'loop'     	=> '',
		'autoplay' 	=> '',
		'preload' 	=> 'none'
		)
	);
	$mediaType = 'audio-media';
}
if( !empty( $wppic_data->video_url ) ){
	$media = wp_video_shortcode( array(
		'src'      	=> $wppic_data->video_url,
		'poster'	=> $wppic_data->live_preview_url,
		'loop'     	=> '',
		'autoplay' 	=> '',
		'preload' 	=> 'none',
		'width' 	=> '590',
		'height' 	=> '332',
		)
	);
	$mediaType = 'video-media';
}


/***************************************************************
 * Start template
 ***************************************************************/
?>
<div class="wp-pic-large" style="display: none;">
	<div class="wp-pic-large-content <?php echo $mediaType ?>">
		
		<?php if ( !empty( $media ) && $mediaType == 'video-media' ): ?> 
			<?php echo $media ?> 
		<?php else : ?> 
			<a class="wp-pic-asset-bg" href="<?php echo $wppic_data->url ?>" target="_blank" title="<?php _e('Visit Product Page', 'wppic-envato') ?>">
				<?php echo $banner ?> 
			</a>
		<?php endif; ?> 

		<div class="wp-pic-half-first">
		
			<?php if ( empty( $banner ) ): ?> 
				<a class="wp-pic-logo" href="<?php echo $wppic_data->url ?>" <?php echo $bgImage ?> target="_blank" title="<?php _e('Visit Product Page', 'wppic-envato') ?>"></a>
			<?php endif; ?> 
			
			<a class="wp-pic-name" href="<?php echo $wppic_data->url ?>" target="_blank" title="<?php _e('Visit Product Page', 'wppic-envato') ?>"><?php echo $wppic_data->name ?></a>
			<p class="wp-pic-updated"><span><?php _e('Last Updated:', 'wppic-envato') ?></span> <?php echo $wppic_data->last_updated ?></p>
		</div>
		<div class="wp-pic-half-last">
			<a class="wp-pic-author-avatar" href="<?php echo $authorUrl ?>" target="_blank" title="<?php echo $wppic_data->author ?>"><img class="wp-pic-avatar" src="<?php echo $wppic_data->avatar ?>" alt="<?php echo $wppic_data->author ?>"></a>
			<p class="wp-pic-author-link"><?php _e('Author:', 'wppic-envato') ?> <a href="<?php echo $authorUrl ?>" target="_blank" title="<?php echo $wppic_data->author ?>"><?php echo $wppic_data->author ?></a></p>
			<p class="wp-pic-author-followers"><?php echo $wppic_data->followers . ' ' . __('Followers', 'wppic-envato') ?></p>
			<p class="wp-pic-author-sales"><?php echo $wppic_data->sales . ' ' . __('Sales', 'wppic-envato') ?></p>
		</div>
		
		<?php if ( !empty( $media ) && $mediaType == 'audio-media' ): ?> 
			<?php echo $media ?> 
		<?php endif; ?> 
		
		<div class="wp-pic-bottom">
			<div class="wp-pic-bar">
				<a href="<?php echo $wppic_data->url ?>" class="wp-pic-rating" target="_blank" title="<?php _e('Visit Product Page', 'wppic-envato') ?>">
					<?php echo $wppic_data->rating ?>%<em><?php _e('Ratings', 'wppic-envato') ?></em>
				</a>
				<a href="<?php echo $wppic_data->url ?>" class="wp-pic-downloaded" target="_blank" title="<?php _e('Visit Product Page', 'wppic-envato') ?>">
					<?php echo $wppic_data->sales ?><em><?php _e('Sales', 'wppic-envato') ?></em>
				</a>
				<a href="<?php echo $wppic_data->url ?>" class="wp-pic-cost" target="_blank" title="<?php _e('Visit Product Page', 'wppic-envato') ?>">
					<sup>$</sup><?php echo $wppic_data->cost ?><em><?php _e('Cost', 'wppic-envato') ?></em>
				</a>
			</div>
		</div>

	</div>
</div>
<?php //end of template