��          �   %   �      `     a     g     }     �     �     �     �     �     �  	   �     �     �  	   �  ?        G     O     X     ^     e     n  !   �  x   �  !        >  6   T  2   �  �  �     ~  "   �     �  
   �     �     �  *   �       &        ;  (   L      u     �  5   �     �     �     	     	     $	  1   7	  )   i	  "  �	  A   �
     �
  6     e   E            
                                                    	                                                            1.0.4 Add an Envato product Author: Back Brice CAPOBIANCO Cost Documentation and examples Envato Envato username Followers Last Updated: More b*web Plugins More info Please refer to the Envato item URL to determine the product ID Ratings Ratings: Sales Sales: Settings Visit Product Page WP Envato Affiliate Card by b*web WP Envato Affiliate Card displays nice cards with Envato API data and lets you add your affiliate ID to get commissions. Your Envato affiliate ID/username http://b-website.com/ http://b-website.com/wp-plugin-info-card-for-wordpress is not a valid item ID. This key has been deleted. Project-Id-Version: WP Envato Affiliate Card by b*web v1.0.4
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2015-09-21 00:00+0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.4
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
Last-Translator: Alex Lom <alextheme2015@mail.ru>
Language: ru_RU
X-Poedit-SearchPath-0: .
 1.0.4 Добавить товар Envato Автор: Назад Brice CAPOBIANCO Стоимость Документация и примеры Envato Имя пользователя Envato Нравится Последнее Обновление: Ещё плагины от b*web Узнать больше ID товара Envato берётся из его URL Рейтинг Рейтинг: Продажи Продажи: Настройки Перейти на страницу товара WP Envato Affiliate Card автор b*web WP Envato Affiliate Card генерирует красивые карточки, используя API Envato для работы с данными, и позволяет Вам добавлять Ваш партнёрский ID, чтобы зарабатывать на комиссии. Ваш ID партнёра Envato/имя пользователя http://b-website.com/ http://b-website.com/wp-plugin-info-card-for-wordpress это неверный ID товара. Данный идентификатор был удалён. 