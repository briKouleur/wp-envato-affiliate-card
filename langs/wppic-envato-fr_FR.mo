��          �      �           	          '     ,     =     B     I  	   Y     c  	   q  ?   {     �     �     �     �     �  !   �  x     !   �     �  6   �  2   �  �  )     �     �     �     �     �          
     #     ,     E  M   T     �     �     �     �     �  "   �  �   �     �     �  6   �  8   		                              	                                                                      
    Add an Envato product Author: Back Brice CAPOBIANCO Cost Envato Envato username Followers Last Updated: More info Please refer to the Envato item URL to determine the product ID Ratings Ratings: Sales Sales: Visit Product Page WP Envato Affiliate Card by b*web WP Envato Affiliate Card displays nice cards with Envato API data and lets you add your affiliate ID to get commissions. Your Envato affiliate ID/username http://b-website.com/ http://b-website.com/wp-plugin-info-card-for-wordpress is not a valid item ID. This key has been deleted. Project-Id-Version: WP Envato Affiliate Card by b*web
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-22 07:04+0000
PO-Revision-Date: 2016-09-22 10:49+0200
Last-Translator: Wp Trads <contact@wolforg.eu>
Language-Team: Wp Trads <contact@wolforg>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 1.8.7.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
Language: fr_FR
X-Poedit-SearchPath-0: .
 Ajouter un produit Envato Auteur : Retour Brice CAPOBIANCO Prix Envato Nom d'utilisateur Envato Abonnés Dernière mise à jour : Plus d’infos Merci de vous référer à l'URL du produit Envato pour en déterminer l'ID : Notes Notes : Ventes Ventes : Voir la page du Produit WP Envato Affiliate Card par b*web WP Envato Affiliate Card affiche de belles cartes avec les données d’API Envato et vous permet d’ajouter votre ID d’affilié pour obtenir des commissions. Votre nom d'utilisateur Envato http://b-website.com/ http://b-website.com/wp-plugin-info-card-for-wordpress n'est pas un ID valide. Cet élément a été supprimé. 