=== WP Envato Affiliate Card by b*web ===
Description: WP Envato Affiliate Card lets you display nice cards about Envato products based on Envato API data, with your affiliate username embedded. Get affiliate cash in the process !
Author: Brice CAPOBIANCO
Envato Username: b-web
Author URI: http://b-website.com/
Plugin URI: http://b-website.com/wp-envato-affiliate-card-powered-envato-market-api
Requires at least: 3.7
Tested up to: 4.6.1
Version: 1.0.10
Tags: API, Envato, Envato API, themeforest, codecanyon, videohive, audiojungle, graphicriver, photodune, 3docean, activeden, plugin, card, dashboard, shortcode, ajax, WordPress, CSS3, rotate, flip card, UX, ui, showcase, themes, theme, template

 
== Description ==

WP Envato Affiliate Card is a premium add-on of the free plugin WPPIC (available in the WordPress repository). 
It lets you display nice cards (3D or flat layout) about Envato products through Envato API data with your affiliate username embedded. Get affiliate cash in the process !
This plugin supports all Envato products types and markets. If you embed an audio product, it will automatically add an audio player. If the included product has a video preview or is a video, the plugin will automatically add a video player. Audio and video players are based on the native WordPress players (HTML5).
The plugin uses simple shortcodes with a nice UI (WYSIWYG button included) to let you include a beautiful card in your content + sidebar!
Supported markets are: themeforest, codecanyon, videohive, audiojungle, graphicriver, photodune, 3docean, activeden.


This plugin is responsive-friendly :
- the flipping card has a fixed with of 280px
- the large layout is fully responsive


= How does it work? =

It uses Envato API to fetch product data. All you need to do is provide a valid product ID, and then insert the shortcode in any page to make it work at once!
This plugin is very light and includes scripts and CSS only if and when required. The shortcode may be added everywhere shortcodes are supported in your theme (content, sidebar widget).
The plugin also uses WordPress transients to store data returned by the API for 12 hours (720min by default), so your page loading time will not be increased due to too many requests. You may change this cache duration and/or load the API data asynchronously with AJAX.
The dashboard widget is very easy to set up: you simply add as many products as you want in the admin page and they become visible in your dashboard. Easily monitor all your Envato product from one central location in your admin area!(Fields are added on-the-fly and are sortable via drag-and-drop.)
Perfect to keep track of your own products!

As a bonus, the plugin uses the TinyMCE API to improve UI and make inserting shortcodes easier.


You may provide a list of slugs (comma-separated) in your shortcode slug parameter, WPPIC will randomly choose an item of the list on each page refresh.
You may easily overload the plugin render. You need to create a new 'wppic-templates' folder into your theme folder, then copy the template you want to overload from WP Envato Affiliate Card 'wppic-templates' folder.
You may create your own template file. You need to create a new 'wppic-templates' folder into your theme folder, then copy the template file "wppic-template-envato.php" or "wppic-template-envato-large.php" from WP Envato Affiliate Card '/wppic-templates' folder. Rename the file like this "wppic-template-envato-NEWTEMPLATE.php", edit it as you go and add your own CSS rules. Finally, call your new template by adding the following parameter in your shortcode: layout="NEWTEMPLATE"

Knonw issues:
Some H.264 encoded videos from the Envato market cannot be played in the Firefox browser. This is a known issue and the only current solution is to play them through a Flash player. However because the render layouts are responsive, we can't load a SWF player as a fallback.

= Shortcode parameters =
Please refer to the WP Plugin Info Card plugin page for full documentation.

* **type:** envato (default: plugin)
* **slug:** envato product ID - Please refer to the product URL on the envato market to determine its ID. Eg: http://themeforest.net/item/item-name/ITEM-ID
* **custom:** value to output : (default: empty)	
	* For Envato products: slug, url, name, market, cost, author, avatar, followers, sales, live_preview_url, thumbnail, rating, downloaded, last_updated, audio_url, video_url


= Examples =

[wp-pic type="envato" slug="9917232"]

[wp-pic type="envato" slug="9917232" align="left" scheme="scheme1" margin="5px 5px 0 0" layout="large" ajax="yes" ]

[wp-pic type="envato" slug="9917232" custom="name" ] has been sold [wp-pic type="envato" slug="662r4437" custom="sales" ] times!


[FULL DOCUMENTATION AND EXAMPLES](http://b-website.com/wp-plugin-info-card-for-wordpress "Documentation & examples")



= Languages =

Plugin is fully translatable through PO & MO files.

Available languages are:

* English
* French
* Deutsch
* Russian


== Installation ==

1. Upload and activate the two plugins "WP Envato Affiliate Card" and "WP Plugin Info Card"
2. Click on the "WP Plugin Info Card" menu
3. Follow instructions, every option is documented ;-)


== Changelog ==

= 1.0.10 - 11/03/2016 =
* French translation update

= 1.0.9 - 08/19/2016 =
* Tested on WP 4.6 with success!
* Fix a warning when WPPIC is not installed

= 1.0.8 - 28/15/2015
* Fix a typo in wppic-templates/wppic-template-envato.php line 10

= 1.0.7 - 11/15/2015 =
* Russian translation added
* Fix a bug on MP3 fetching
* Better respect WordPress Coding standards

= 1.0.6 - 04/16/2015 =
* Fix a PHP warning if the product does not exists
* Tested on WP 4.2 with success!
* readme.txt update

= 1.0.5 - 01/14/2015 =
* Deutsch translation added
* Minor security improvements

= 1.0.4 - 01/11/2015 =
* Fix date render on dashboard widget

= 1.0.3 - 01/09/2015 =
* Minor PHP improvements
* Remove logo from meta
* Date internationalization
* Templates update for better date support

= 1.0.2 - 01/04/2015 =
* Minor CSS fixes on card layout (backface visibility problem)
* Readme update

= 1.0.1 - 12/30/2014 =
* Minor PHP fixes
* CSS update
* Minified CSS file added
* Translation update

= 1.0 =
* First release.